# FORD 1

### Date: 2023.05.08

### M/N: 200272


ATO S/N: JT3230508

---

| FLT  | 2x UH-1H  |
|----|----------|
| AB | FARP MADRID | 
| PARK | - |
| TASK | TRANSPORT |
| AO | HATAY |

---

## COMMS

### Intraflight

|Type|Freq (MHz AM)|
|-|-|
| **PRIMARY** | **37**  |
| SECONDARY | 111 |


### Control ladder

| Role | Freq  (MHz AM) | Code |
|-|-|-|
| HATAY TOWER | 259.550 | BLUE1 |
| GCI 1 | 55.500 | BLUE2 |
| REPORT | 251.050 | BLACK1 |


---


## SPINS/REMARKS
MEZ: 358 kHz    -  BAR: 113.90 MHz

LZ1: Fly 177 for 28nm. 
MEZ 172 BAR 267
LZ1 at town Serghaya

LZ1: Fly 222 from LZ1 for 10nm
MEZ 143 BAR 282
LZ2 on ridge west of Madaya

LZ3: Fly 207 for 4nm from LZ2
MEZ 128 BAR 298
LZ3 on northern edge of canyon, Syrian-Lebanon border 







