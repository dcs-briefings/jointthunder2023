# COLT 3

### Date: 2023.05.08

### M/N: 20053


ATO S/N: JT3230508

---

| FLT  | 4x F-16  |
|----|----------|
| AB | INCIRLIK 05 21X 109.30 55 | 
| PARK | GOLF LOOP 09, 10, 11, 12 |
| TASK | BARCAP |
| AO | AUGUSTINER |

---

## COMMS

### Intraflight

|Type|Freq (MHz AM)|
|-|-|
| **PRIMARY** | **143.725**  |
| SECONDARY | 139.475 |
| **IFF M3** | 5171-5174 |

### Control ladder

| Role | Freq  (MHz AM) | Code |
|-|-|-|
| Incirlik Ground | 309.250 | BEIGE1 |
| Incirlik Tower | 360.100 | YELLOW3 |
| GCI AOR1 | 257.950 | ORANGE2 |
| GCI AOR2 |  260.150 | BLUE7 |
| REPORT | 251.050 | BLACK1 |


---


## SPINS/REMARKS

HOLDING AUGUSTINER - COMMIT AUTH in AOR Radius 40nm

BLOCK: 290-340

TANKER: AAR NEBRASKA [TEXACO2 210 252.1 115Y, SHELL2 180 240.5 120Y]





