# DODGE 2

### Date: 2023.06.19

### M/N: 20360


ATO S/N: JT6230619

---

| FLT  | 4x F-16  |
|----|----------|
| AB | RAMAT DAVID 29 TO 33 LND 84X 111.10 326 | 
| PARK | 18, 19, 16, 17 |
| TASK | STRIKE |
| AO | SAYQAL AB |

---

## COMMS

### Intraflight

|Type|Freq (MHz AM)|
|-|-|
| **PRIMARY** | **143.725**  |
| SECONDARY | 140.600 |


### Control ladder

| Role | Freq  (MHz AM) | Code |
|-|-|-|
| RAMAT DAVID GND/TWR | 250.450 | GRAY6 |
| GCI AOR1 | 257.950 | PINK4 |
| GCI AOR2 | 260.150 | GOLD5 |
| REPORT | 251.050 | BLACK1 |
| PACKAGE | 142.600 | 142.225 |

---


## SPINS/REMARKS
Y 92

WPT4-5 (MIDWAY) - GOLD5 CHECK-IN

WPT6 - IP

WPT7 - TGT

TARGET ASSIGNMENT
| EL | PRIMARY| SECONDARIES |
|-|-|-|
| 1-1 | **WP 13** - TGT 6 | **WP 8, 9** TGT 1, 2   |
| 1-1 | **WP 14** - TGT 7 | **WP 10, 11** TGT 3, 4   |
| 1-1 | **WP 15** - TGT 8 | **WP 12, 21** TGT 5, 14   |
| 1-1 | **WP 16** - TGT 9 | **WP 22, 23** TGT 15, 16   |

---



