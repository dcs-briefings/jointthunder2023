# COLT 4

### Date: 2023.06.05

### M/N: 200546


ATO S/N: JT5230605

---

| FLT  | 3x F-16  |
|----|----------|
| AB | INCIRLIK 05 21X 109.30 55 | 
| PARK | GOLF LOOP 13-16 |
| TASK | STRIKE |
| AO | ACP PARIS (INGRESS) |

---

## COMMS

### Intraflight

|Type|Freq (MHz AM)|
|-|-|
| **PRIMARY** | **140.700**  |
| SECONDARY | 141.925 |


### Control ladder

| Role | Freq  (MHz AM) | Code |
|-|-|-|
| Incirlik Ground | 309.250 | BEIGE1 |
| Incirlik Tower | 360.100 | YELLOW3 |
| GCI AOR1 | 257.950 | ORANGE2 |
| GCI AOR2 | 260.150 | BLUE7 |
| REPORT | 251.050 | BLACK1 |


---


## SPINS/REMARKS

WPT3 - INGRESS - BLUE7 CHECK IN

WPT5 - FENCE/IFF

LASER CODES: 1621-1622-1623-1624

TANKER: AAR MINNESOTA [TEXACO1 210 255.5 124Y, SHELL1 180 241.3 121Y]





