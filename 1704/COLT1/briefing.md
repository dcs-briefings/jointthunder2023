# COLT 1

### Date: 2023.04.17

### M/N: 200544


ATO S/N: JT1230417

---

| FLT  | 2x F-16  |
|----|----------|
| AB | INCIRLIK 05 21X 109.30 55 | 
| PARK | GOLF LOOP 09, 10 |
| TASK | BARCAP |
| AO | MENABREA |

---

## COMMS

### Intraflight

|Type|Freq (MHz AM)|
|-|-|
| **PRIMARY** | **143.775**  |
| SECONDARY | 143.525 |


### Control ladder

| Role | Freq  (MHz AM) | Code |
|-|-|-|
| Incirlik Ground | 309.250 | BEIGE1 |
| Incirlik Tower | 360.100 | YELLOW3 |
| GCI AOR1 | 257.950 | ORANGE2 |
| GCI AOR2 |  260.150 | BLUE7 |
| REPORT | 251.050 | BLACK1 |


---


## SPINS/REMARKS

HOLDING MENABREA - COMMIT AUTH in AOR Radius 40nm

BLOCK: 290-340

TANKER: AAR NEBRASKA [TEXACO2 210 252.1 115Y, SHELL2 180 240.5 120Y]





