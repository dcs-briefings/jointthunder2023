# COLT 6

### Date: 2023.04.17

### M/N: 200548


ATO S/N: JT1230417

---

| FLT  | 2x F-16  |
|----|----------|
| AB | INCIRLIK 05 21X 109.30 55 | 
| PARK | GOLF LOOP 21, 22 |
| TASK | SEAD |
| AO | PAULANER |

---

## COMMS

### Intraflight

|Type|Freq (MHz AM)|
|-|-|
| **PRIMARY** | **140.350**  |
| SECONDARY | 141.525 |


### Control ladder

| Role | Freq  (MHz AM) | Code |
|-|-|-|
| Incirlik Ground | 309.250 | BEIGE1 |
| Incirlik Tower | 360.100 | YELLOW3 |
| GCI AOR1 | 257.950 | ORANGE2 |
| GCI AOR2 |  260.150 | BLUE7 |
| REPORT | 251.050 | BLACK1 |


---


## SPINS/REMARKS

HOLDING PAULANER - COMMIT AUTH in AOR Radius 40nm

BLOCK: 290-340

TANKER: AAR MINNESOTA [TEXACO1 210 255.5 124Y, SHELL1 180 241.3 121Y]





