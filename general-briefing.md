## Pre-mission

- Check the ATO mission board (google spreadsheet) to see your tasking. Note other friendly flights operating in your AO/holding. This page will also list any primary targets, remarks, restrictions and final battlespace controller.
- Check your flight on the combined ops mission planner to see your flight path, situation on the map, access the MDC etc. It is important to check:
    - Control space boundaries
    - IFF IN/OUT line
    - Bullseye position (AWACS **will be using bullseye calls** so make sure you have SA on it's position before the mission)

### Pre-mission prep
I highly recommend using open kneeboard, or kneeboard builder if you prefer to make sure you have the MDC, briefing page and LTAG charts (if operating out of Incirlik) is on hand during the mission. LTAG charts are pinned in our discord channel. Everything else can be found in this git repository. 

Make sure everything is up to date and working pre-mission. SRS, DCS etc. SRS functionality is essential.

## Briefing

-The briefing will commence at 1900 UTC (our regular mission time) on the AMVI discord. After the briefing, we will be given a few minutes to brief our flights before the 
server is put up, which we will do in the briefing channel of our discord. Once the server is up, join ASAP as startup to takeoff time is usually quite short.

## Pre-startup

- Before starting your aircraft, contact ground and request startup (in the F-16 you can enable battery or ground power and turn on the backup radio in front of the throttle.)
If there's no answer on ground after a few minutes, startup anyway if time is short.

## Startup/Taxi

- Remember to upload the relevant data cartridge and set the requested IFF code. (Currently via IFF panel on the left of the cockpit in the F-16, until the functionality is added to the DED)

- Request taxi from ground when ready, Specifying flight call-sign and position. Do this earlier than needed as there are often quite long queues for the runway. Take off time of your flight plan does not need to be respected, but time on station does. (Usually +/- 5 minute leeway )

## Takeoff/climb-out

- After take-off, once you get to your ACP (usually Mars or Venus) report it to Tower and they'll clear you to contact AWACS. (Orange2 for outbound Incirlik flights).

- Perform a "pre-briefed" check-in with AWACS, with the following information :
    - Flight call-sign
    - Mission Number
    - Position (eg, Waypoint Venus. Don't use your flightplan waypoint numbers as they'll have no idea what your flightplan actually is )
    - Flight Level
    - "Checking in as fragged"  

## Mission

- Remain within the safe air corridors during cruise and at the assigned altitudes from your MDC

- When approaching an AWACS control boundary (e.g from Orange2 to Blue7) request permission from current AWACS to switch frequencies.
- Once cleared, contact the new controller and perform the check-in brief again. This needs to be done whenever transitioning between AWACS airspaces.

- Remember to **turn OFF the IFF system** when crossing the IFF IN/OUT line (and turn it back **ON** when RTBing and crossing the line)

- Inform AWACS if you're going to be more than 5 minutes late to your AO with a rolex call
    > "Overlord, Colt1 rolex plus 6 minutes."


- Remember that friendly flights will be operating in your area and your IFF system is OFF. You should have datalink IFF, but always ask AWACS to declare unknowns if you think they are enemy, and unless acting in self defense, request permission to engage any hostiles. As always, if in doubt, **don't shoot.**

- Any weapon releases should be announced on the AWACS frequency. 

    > "Colt 6, Magnum, SA-15, bullseye two five six forty three"

## RTB and Inbound
- During RTB, contact the INFLIGHT REPORT channel and report the outcome of the mission in the following format:

    - Call-sign 
    - Mission Number
    - AO Location
    - ToS/ToT
    - Results (e.g 2 hostile mig-29 splashed, 1 friendly F-16 KIA )
    - Remarks (any pertinent information from the mission, if applicable )

- Be sure to follow the same check-in procedure with AWACS as you make your way back along your flight path

- Contact tower no later than 50 miles out from homeplate and state  intentions. You can just say "inbound for landing runway 05", and wait for instructions from ATC (they will usually put you on the ILS STAR) otherwise you can request a specific approach (visual approach runway 5, ILS landing runway 5, overhead break for 05, etc)

- Remember to contact ground when transferred by TWR. You will be given taxi instructions to your original parking

- After shutdown, de-brief flight in our discord.














