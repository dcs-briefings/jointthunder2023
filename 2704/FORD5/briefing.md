# FORD 5

### Date: 2023.04.27

### M/N: 2002109


ATO S/N: JT2230427

---

| FLT  | 2x UH-1H  |
|----|----------|
| AB | HATAY 11 | 
| PARK | 06 |
| TASK | TRANSPORT |
| AO | HATAY |

---

## COMMS

### Intraflight

|Type|Freq (MHz AM)|
|-|-|
| **PRIMARY** | **37**  |
| SECONDARY | 111 |


### Control ladder

| Role | Freq  (MHz AM) | Code |
|-|-|-|
| HATAY TOWER | 259.550 | BLUE1 |
| GCI 1 | 257.950 | ORANGE2 |
| GCI 2 | 55.500 | BLUE2 |
| REPORT | 251.050 | BLACK1 |


---


## SPINS/REMARKS
WP1 - WP2: 134 - 9 nm
WP2 - WP3: 118 for 7.4 nm (307 for 16.6 nm to Hatay)
WP3 - WP4 021 for 20.2 nm (247 for 22.3 nm to Hatay)





